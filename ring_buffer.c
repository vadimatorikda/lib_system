#include "ring_buffer.h"
#include <string.h>
#include "project_cfg.h"

bool buf_is_empty (buf_t *b) {
    return b->used == 0;
}

bool buf_is_full (buf_t *b) {
    return b->used == b->len;
}

uint8_t buf_get_number_items (buf_t *b) {
    return b->used;
}

void buf_get_item (buf_t *b, void *item) {
    if (b->used == 0) {
        HARD_FAULT();
    }

    uint8_t *buf_start = b->buf;
    uint8_t *src = &buf_start[b->start * b->item_size];
    memcpy(item, src, b->item_size);

    b->start++;
    if (b->start == b->len) {
        b->start = 0;
    }

    b->used--;
}

void buf_add_item (buf_t *b, void *item) {
    if (b->used == b->len) {
        HARD_FAULT();
    }

    uint8_t *buf_start = b->buf;
    uint8_t *dst = &buf_start[b->end * b->item_size];
    memcpy(dst, item, b->item_size);

    b->end++;
    if (b->end == b->len) {
        b->end = 0;
    }

    b->used++;
}
