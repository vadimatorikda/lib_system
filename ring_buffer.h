#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    void *buf;         // Buffer for all items.
    uint8_t item_size; // Size of byte in one item.
    uint8_t len;       // Max items.

    // Internal data. Default value 0.
    uint8_t used;      // Number of used items.
    uint8_t start;
    uint8_t end;
} buf_t;

#define BUF_SYSTEM_PART .used=0,.start=0,.end=0

bool buf_is_empty (buf_t *b);
bool buf_is_full (buf_t *b);
uint8_t buf_get_number_items (buf_t *b);
void buf_get_item (buf_t *b, void *item);
void buf_add_item (buf_t *b, void *item);

#endif // RING_BUFFER_H
